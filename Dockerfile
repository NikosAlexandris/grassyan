FROM debian:testing

LABEL title='grassyan'
LABEL version='3.0'

LABEL description="grassyan: a recipe, based on debian, to build a GRASS GIS \
docker image and deploy in JRC's JEODPP nodes"

LABEL note="Pronounced 'grassian', a Dockerfile named after its first author \
Yann Chemin <yann.chemin@gmail.com>"

LABEL maintainer='Nikos Alexandris <nik@nikosalexandris.net>'
LABEL "is.alexandr"='Nikos Alexandris'
LABEL "is.alexandr.vendor"='Nikos Alexandris'

LABEL disclaimer="This Dockerfile is maintained by Nikos Alexandris in his \
personal capacity. Its configuration does not reflect any view, nor it is \
endorsed by the JRC Earth Observation Data Processing Platform (JEODPP) or \
the Joint Reseatch Center (JRC)"

# system environment

ENV DEBIAN_FRONTEND='noninteractive' \
    TERM='xterm' \
    DISPLAY=':1.0' \
    LC_ALL='C.UTF-8' \
    USERNAME='grassyan'
ENV HOME="/home/$USERNAME"

# hardcoded grassY variables

ENV OSGEO='osgeo' \
    SCRIPTS='scripts' \
    GRASSDB='grassdb' \
    \
    GRASS_PAGER=cat \
    GRASS_WISH=wish \
    GRASS_MESSAGE_FORMAT=standard \
    \
    GRASS_BACKGROUNDCOLOR=220:220:220 \
    GRASS_RENDER_IMMEDIATE=cairo \
    GRASS_RENDER_TRUECOLOR=TRUE \
    GRASS_RENDER_TRANSPARENT=TRUE \
    GRASS_RENDER_WIDTH=1000 \
    GRASS_RENDER_HEIGHT=1000 \
    GRASS_RENDER_FILE_READ=TRUE \
    GRASS_RENDER_FILENAME='grass_render_file.png' \
    \
    GRASS_PNG_AUTO_WRITE=TRUE \
    GRASS_PNG_COMPRESSION=9 \
    GRASS_PNG_MAPPED=TRUE \
    GRASS_PNG_READ=TRUE \
    \
    GRASS_COMPRESSOR=ZSTD \
    GRASS_ZLIB_LEVEL=1 \
    \
    TEMPORARY_LOCATION='Temporary_Location' \
    \
    GRASS_BRANCH='grasstrunk' \
    GRASS_BRANCH_URL='https://svn.osgeo.org/grass/grass/trunk' \
    GRASS_BINARY_DIRECTORY='bin.x86_64-pc-linux-gnu' \
    GISBASE_DIRECTORY='dist.x86_64-pc-linux-gnu' \
    GRASS_COMPILATION_SCRIPT='compile_grass_trunk.sh' \
    GRASS_ADDONS='grass-addons' \
    GRASS_ADDONS_SOURCE='https://svn.osgeo.org/grass/grass-addons' \
    \
    GITHUB_URL='https://github.com/NikosAlexandris' \
    GITLAB_URL='https://gitlab.com/NikosAlexandris' \
    I_LANDSAT_IMPORT='i.landsat.import' \
    I_LANDSAT8_SWLST='i.landsat8.swlst' \
    LST_TIME_SERIES_SCRIPTS='lst_time_series.zip'

ENV OSGEO_DIRECTORY="$HOME/$OSGEO"
ENV GRASS_SOURCE_PATH="$OSGEO_DIRECTORY/$GRASS_BRANCH"
ENV GRASS_GISBASE_PATH="$GRASS_SOURCE_PATH/$GISBASE_DIRECTORY" \
    GRASS_ADDON_PATH="$OSGEO_DIRECTORY/$GRASS_ADDONS"
ENV GRASS_ADDON_BASE="$GRASS_ADDON_PATH" \
    GRASS_IMAGERY_ADDONS_PATH="$GRASS_ADDON_PATH/grass7/imagery" \
    I_LANDSAT_IMPORT_REPOSITORY="${I_LANDSAT_IMPORT}.git" \
    I_LANDSAT8_SWLST_REPOSITORY="${I_LANDSAT8_SWLST}.git" \
    SCRIPTS_PATH="$OSGEO_DIRECTORY/$SCRIPTS"

ENV GRASS_DATA_BASE="$HOME/$GRASSDB"
ENV GRASS_RENDER_DIRECTORY="$GRASS_DATA_BASE/render"
ENV GRASS_RENDER_FILE="$GRASS_RENDER_DIRECTORY/$GRASS_RENDER_FILENAME"

# GRASS GIS' compilation dependencies

# faster package configuration
RUN apt-get update && apt-get install apt-utils -y \
&&  apt-get upgrade -y \
&&  apt-get dist-upgrade -y \
&&  apt-get install -y apt \
&&  apt-get install -y \
        time \
        bc \
        flex \
        byacc \
        m4 \
        automake \
        cmake \
        ccache \
        gawk \
        grep \
        sed \
        libreadline-dev \
        bash \
        subversion \
        imagej \
        dillo \
        links \
        byobu \
        curl \
        git \
        htop \
        mc \
        unzip \
        vim \
        wget \
        git \
        libxml2-dev \
        python \
        build-essential \
        make \
        man \
        gcc \
        python-dev \
        locales \
        python-pip \
        xauth \
        xvfb \
        python-dateutil \
        libgsl0-dev \
        python-opengl \
        python-wxversion \
        python-wxtools \
        python-wxgtk3.0 \
        python-dateutil \
        libgsl0-dev \
        wx3.0-headers \
        wx-common \
        libwxgtk3.0-dev \
        libwxbase3.0-dev \
        zlib1g-dev \
        libzstd-dev \
        libtiff-dev \
        libgeotiff-dev \
        libpnglite-dev \
        libcairo2-dev \
        libsqlite3-dev \
        libpq-dev \
        libfreetype6-dev \
        gettext \
        ghostscript \
        libboost-thread-dev \
        libboost-program-options-dev \
        libjpeg-dev \
        libavutil-dev \
        ffmpeg2theora \
        libffmpegthumbnailer-dev \
        libavcodec-dev \
        libxmu-dev \
        libavformat-dev \
        libswscale-dev \
        libglu1-mesa-dev \
        libxmu-dev \
        python-gdal \
        liblapack-dev \
        python-numpy \
        libfftw3-double3 \
        libfftw3-dev \
        libcairo2 \
        libsqlite3-dev \
        libnetcdf-dev \
        libtinfo-dev \
&& apt-get install -y grass-dev \
&& apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*  # --- Replace ::rm -rf:: --- #

RUN dpkg-reconfigure locales \
    && locale-gen C.UTF-8 \
    && /usr/sbin/update-locale LANG=C.UTF-8 \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && dpkg-reconfigure locales

# create USER

RUN useradd --create-home --shell /bin/bash "$USERNAME" \
&&  echo "${USERNAME}:newpassword" | chpasswd
# -------  What to do here? -------- ^^^^^^^^

# directory for source code

# set up a GRASS GIS data base directory

WORKDIR $HOME
RUN pwd

RUN mkdir -p $GRASSDB \
&&  chown $USERNAME $GRASSDB \
&&  chmod -R a+rwx "$GRASSDB"

RUN mkdir -p "$GRASS_RENDER_DIRECTORY" \
&&  chown $USERNAME "$GRASS_RENDER_DIRECTORY" \
&&  mkdir -p "$OSGEO_DIRECTORY" \
&&  chown $USERNAME "$OSGEO_DIRECTORY"

# checkout GRASS GIS (and add-ons) as "USERNAME"

USER $USERNAME
WORKDIR "$OSGEO_DIRECTORY"
RUN svn checkout "$GRASS_BRANCH_URL" "$GRASS_BRANCH"
# RUN svn checkout $GRASS_ADDONS_SOURCE "$GRASS_ADDONS"

# transfer compilation script

ADD "$GRASS_COMPILATION_SCRIPT" "$OSGEO_DIRECTORY"

# compile GRASS GIS

WORKDIR "$GRASS_SOURCE_PATH"
RUN bash "../${GRASS_COMPILATION_SCRIPT}"

# soft-linking as root

USER root

# instead of `make install`

RUN ln -s "$GRASS_SOURCE_PATH/$GRASS_BINARY_DIRECTORY/grass77" \
    /usr/local/bin/grass77 \
&&  ln -s /usr/local/bin/grass* \
    /usr/local/bin/grass  # `grass` command regardless of version

# switch back to "USERNAME"

USER $USERNAME
RUN echo "\nexport TERM=xterm" >> "$HOME/.bashrc"
# RUN Xvfb :1 -screen 0 1024x768x16 &> xvfb.log  &

# install Landsat 8 related Addons

RUN mkdir -p "$GRASS_IMAGERY_ADDONS_PATH"

WORKDIR "$GRASS_IMAGERY_ADDONS_PATH"
RUN git clone "$GITHUB_URL/${I_LANDSAT_IMPORT_REPOSITORY}"
WORKDIR "$GRASS_IMAGERY_ADDONS_PATH/$I_LANDSAT_IMPORT"
RUN make MODULE_TOPDIR="$GRASS_SOURCE_PATH"

WORKDIR "$GRASS_IMAGERY_ADDONS_PATH"
RUN git clone "$GITHUB_URL/${I_LANDSAT8_SWLST_REPOSITORY}"
WORKDIR "$GRASS_IMAGERY_ADDONS_PATH/$I_LANDSAT8_SWLST"
RUN make MODULE_TOPDIR="$GRASS_SOURCE_PATH"

# transfer scripts for lst time series
RUN mkdir -p "$SCRIPTS_PATH"
ADD "$LST_TIME_SERIES_SCRIPTS" "$SCRIPTS_PATH"

# declare volume late so permissions apply

VOLUME $GRASSDB
WORKDIR $GRASS_DATA_BASE

# default command ?

CMD ["bash"]
ENV DEBIAN_FRONTEND=teletype

# test!

RUN grass -text -e \
    -c EPSG:4326 \
    "$GRASSDB/$TEMPORARY_LOCATION" \
&&  grass -text \
    "$GRASSDB/$TEMPORARY_LOCATION/PERMANENT" \
    --exec g.region -p \
&&  mv "$GRASSDB/$TEMPORARY_MAPSET" /tmp
