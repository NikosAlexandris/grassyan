#!/bin/bash

#svn checkout
#svn checkout https://svn.osgeo.org/grass/grass/trunk grass7_trunk

# clean
time make distclean

# update
time svn up

# taken from <http://lists.osgeo.org/pipermail/grass-user/2013-May/068229.html>
renice +17 -p $$

# configure
CFLAGS="-g -O1 -march=native -Wall -Werror-implicit-function-declaration -fno-common -fexceptions -Wreturn-type"
LDFLAGS="-Wl,--no-undefined -Wl,-z,relro,-s"
./configure \
    --with-cxx \
    --with-readline \
    --with-regex \
    --with-nls \
    --with-includes=/usr/include/ --with-libs=/usr/lib64/ \
    --with-proj \
    --with-proj-includes=/usr/include/ \
    --with-proj-libs=/usr/lib64/ \
    --with-proj-share=/usr/share/proj/ \
    --with-geos \
    --with-geos=/usr/bin/geos-config \
    --with-gdal=/usr/bin/gdal-config \
    --with-x \
    --with-motif \
    --with-cairo \
    --with-opengl-libs=/usr/include/GL \
    --without-ffmpeg \
    --with-python=yes --with-python=/usr/bin/python2.7-config \
    --with-wxwidgets \
    --with-freetype=yes --with-freetype-includes="/usr/include/freetype2/" \
    --with-sqlite=yes \
    --with-openmp --with-pthread \
    --with-lapack \
    --with-fftw \
    --with-jpeg \
    --with-tiff \
    --with-png \
    --enable-largefile=yes \
    --with-zstd \
    2>&1 | tee config_log.txt
    # --with-bzlib \  # Fails to find bzlib includes

# Left out --------------------------------------------------------------------
# ./configure \
# --with-odbc=yes \
# --with-mysql=yes --with-mysql-includes="/usr/include/mysql" --with-mysql-libs=/usr/lib/mysql \
# --with-postgres=yes --with-postgresql=yes --with-postgres-includes="/usr/include/postgresql" \
# --with-opencl --with-opencl-includes=/usr/include/CL/ \
# --with-netcdf \
# --without-opendwg \
# Left out --------------------------------------------------------------------

# make
time make -j4

# adopted from Yann's script
# ARCH=`grep ARCH config.status | cut -d'%' -f3 |head -1`
# echo " GRASS GIS start script is in: ./$BINDIR/bin.$ARCH"
# echo " GRASS GIS binaries are in: ./$PREFIX/dist.$ARCH"
echo "(no need to run 'make install')"
echo "Enjoy."
