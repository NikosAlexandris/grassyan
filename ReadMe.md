# grassyan

"grassyan" is a debian-based GRASS GIS docker image (future targeting JRC's
JEODPP cluster nodes). Pronounced "grassian", named after the source
Dockerfile's first author's first name, Yann Chemin. A warm Thank You to Yann
for all of his contributions.

# Specifications

* Latest trunk source code
* SQLite only
* ZSTD (Zstandard) provides compression ratios higher than ZLIB but lower than
BZIP2 (for large data). ZSTD compresses up to 4x faster than ZLIB, and usually
decompresses 6x faster than ZLIB. ZSTD is the recommended default compression
method.
[see https://grass.osgeo.org/grass75/manuals/rasterintro.html#raster-compression]
* `r.buildvrt` - Build a VRT (Virtual Raster) from the list of input raster
maps. [see https://grass.osgeo.org/grass75/manuals/r.buildvrt.html]

# Addons

* `i.landsat.import` - Import Landsat scene(s) in GRASS GIS' data base as
independent Mapset(s) [see https://gitlab.com/NikosAlexandris/i.landsat.import]
* `i.landsat8.swlst` - Practical split-window algorithm estimating Land Surface
Temperature from Landsat 8 TIRS imagery [see
https://grass.osgeo.org/grass74/manuals/addons/i.landsat8.swlst.html]

# Notes

The compilation instruction, in the file `compile_grass_trunk.sh`, is:
```bash
time make -j4
```

Please edit this beforehand as required.

# Author

Nikos Alexandris

# Disclaimer

This Dockerfile is maintained by its author in his personal capacity.
Its configuration does not reflect any view, nor it is endorsed by the JRC
Earth Observation Data Processing Platform (JEODPP) or the Joint Reseatch
Center (JRC).

